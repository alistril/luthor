import ply.lex as lex
from errors import LexException

class Lexer:
    reserved = {
       'AND' : 'AND',
       'OR' : 'OR',   
    }

    # List of token names.   This is always required
    tokens = [
       'BELONGS',
       'NUMBER',
       'STRING',
       'IDENTIFIER',
       'EQUAL',
       'GREATER_THAN',
       'LESS_THAN',
       'GREATER_THAN_OR_EQUAL',
       'LESS_THAN_OR_EQUAL',
       'LPAREN',
       'RPAREN',   
    ] + list(reserved.values())


    # Regular expression rules for simple tokens
    t_BELONGS    = r'\.'
    t_EQUAL    = r'\='
    t_GREATER_THAN   = r'>'
    t_LESS_THAN   = r'\<'
    t_GREATER_THAN_OR_EQUAL  = r'\>\='
    t_LESS_THAN_OR_EQUAL  = r'\<\='
    t_LPAREN  = r'\('
    t_RPAREN  = r'\)'

    def t_COMMENT(self,t):
        r'\#.*'
        pass
        
    # A regular expression rule with some action code
    def t_NUMBER(self,t):
        r'\d+'
        t.value = int(t.value)    
        return t
        
    # A regular expression rule with some action code
    def t_STRING(self,t):
        r'"[a-zA-Z0-9_ @#$%&]*"'
        t.value = t.value[1:len(t.value)-1]
        return t

    def t_IDENTIFIER(self,t):
        r'[a-zA-Z_]+[a-zA-Z0-9_]*'     
        t.type = self.reserved.get(t.value,'IDENTIFIER')    # Check for reserved words
        return t
        

    # Define a rule so we can track line numbers
    def t_newline(self,t):
        r'\n+'
        t.lexer.lineno += len(t.value)
        


    # A string containing ignored characters (spaces and tabs)
    t_ignore  = ' \t'

    # Error handling rule
    def t_error(self,t):
        raise LexException("Illegal character '%s'" % t.value[0])
        t.lexer.skip(1)

    # Compute column. 
    #     input is the input text string
    #     token is a token instance
    def find_column(self,input,token):
        last_cr = input.rfind('\n',0,token.lexpos)
        if last_cr < 0:
            last_cr = 0
        column = (token.lexpos - last_cr) + 1
        return column
        
    # Build the lexer
    def build(self,**kwargs):
        self.lexer = lex.lex(module=self,optimize=1, **kwargs)
    
    # Test it output
    def test(self,data):
        self.lexer.input(data)
        while True:
             tok = self.lexer.token()
             if not tok: break
             print tok
    
