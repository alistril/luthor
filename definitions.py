class Constant(object):
    def luthor_equals(self,side1,side2):
        return str(side1.leaf) + " = " + str(side2.leaf)
        
    def luthor_less_than(self,side1,side2):                
        return str(side1.leaf) + " < " + str(side2.leaf)
            
    def luthor_less_than_or_equal(self,side1,side2):        
        return str(side1.leaf) + " <= " + str(side2.leaf)
            
    def luthor_greater_than(self,side1,side2):        
        return str(side1.leaf) + " > " + str(side2.leaf)
            
    def luthor_greater_than_or_equal(self,side1,side2):        
        return str(side1.leaf) + " >= " + str(side2.leaf  )
            
    def luthor_render_group(self,node,group):
        ret="Other conditions:\n"
        for comparison in group["list"]:
            ret+= "- " + str(comparison.speech) + "\n"
        return ret
