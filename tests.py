from lexing import Lexer
from parsing import Parser
from interpreting import Interpreter
from errors import SemanticException,RenderException
from tree import Node,NodeBELONGS,NodeIDENTIFIER,NodeEQUAL,NodeGREATER_THAN_OR_EQUAL,NodeGREATER_THAN,NodeLESS_THAN_OR_EQUAL,NodeLESS_THAN,NodeNUMBER,NodeSTRING,NodeAND,NodeOR,NodeBoolOp
from definitions import Constant

# Test it out
data = '''
#hello world
OMPSG.competition.score
'''

import unittest

class Competition(object):
    def luthor_equals(self,side1,side2):        
        if isinstance(side1,NodeBELONGS) and isinstance(side2,NodeBELONGS):
            if side1.master().element_name!=side2.master().element_name:
                raise RenderException(RenderException.COULD_NOT_GROUP,"could not resolve equality \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name))
                return "could not resolve equality \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
            else:
                if side1.data_type=="NUMBER" and side1.data_type=="NUMBER":
                    return "tie between %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
                elif side1.data_type=="STRING" and side1.data_type=="STRING":
                    #raise RenderException("not possible that to teams have the same best player") #this is probably not possible
                    return "%s and %s have the same best player" % (side1.selection().master().element_name,side2.selection().master().element_name)
        elif isinstance(side1,NodeBELONGS) and isinstance(side2,NodeNUMBER):
            return "score of %s = %d" % (side1.selection().master().element_name,side2.leaf)
        elif isinstance(side1,NodeNUMBER) and isinstance(side2,NodeBELONGS):
            return "score of %s = %d" % (side2.selection().master().element_name,side1.leaf)
        elif isinstance(side1,NodeBELONGS) and isinstance(side2,NodeSTRING):
            return "best player of %s is %s" % (side1.selection().master().element_name,side2.leaf)            
        elif isinstance(side1,NodeSTRING) and isinstance(side2,NodeBELONGS):
            return "best player of %s is %s" % (side2.selection().master().element_name,side1.leaf)
        else:
            raise RenderException(RenderException.OTHER,"invalid types")
    
    def luthor_less_than(self,side1,side2):        
        if isinstance(side1,NodeBELONGS) and isinstance(side2,NodeBELONGS):
            if side1.master().element_name!=side2.master().element_name:
                raise RenderException(RenderException.COULD_NOT_GROUP,"could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name))
                return "could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
            else:
                if side1.data_type=="NUMBER" and side1.data_type=="NUMBER":
                    return "%s wins against %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
                elif side1.data_type=="STRING" and side1.data_type=="STRING":
                    raise RenderException(RenderException.OTHER,"invalid comparison")
                    
        elif isinstance(side1,NodeBELONGS) and isinstance(side2,NodeNUMBER):
            return "score of %s < %d" % (side1.selection().master().element_name,side2.leaf)
        elif isinstance(side1,NodeNUMBER) and isinstance(side2,NodeBELONGS):
            return "score of %s < %d" % (side2.selection().master().element_name,side1.leaf)        
        else:
            raise RenderException(RenderException.OTHER,"invalid types")
            
    def luthor_less_than_or_equal(self,side1,side2):        
        if isinstance(side1,NodeBELONGS) and isinstance(side2,NodeBELONGS):
            if side1.master().element_name!=side2.master().element_name:
                raise RenderException(RenderException.COULD_NOT_GROUP,"could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name))
                return "could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
            else:
                if side1.data_type=="NUMBER" and side1.data_type=="NUMBER":
                    return "%s wins or at least equalizes against %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
                elif side1.data_type=="STRING" and side1.data_type=="STRING":
                    raise RenderException(RenderException.OTHER,"invalid comparison")
                    
        elif isinstance(side1,NodeBELONGS) and isinstance(side2,NodeNUMBER):
            return "score of %s <= %d" % (side1.selection().master().element_name,side2.leaf)
        elif isinstance(side1,NodeNUMBER) and isinstance(side2,NodeBELONGS):
            return "score of %s <= %d" % (side2.selection().master().element_name,side1.leaf)        
        else:
            raise RenderException(RenderException.OTHER,"invalid types")
            
    def luthor_greater_than(self,side1,side2):        
        if isinstance(side1,NodeBELONGS) and isinstance(side2,NodeBELONGS):
            if side1.master().element_name!=side2.master().element_name:
                raise RenderException(RenderException.COULD_NOT_GROUP,"could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name))
                return "could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
            else:
                if side1.data_type=="NUMBER" and side1.data_type=="NUMBER":
                    return "%s loses against %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
                elif side1.data_type=="STRING" and side1.data_type=="STRING":
                    raise RenderException(RenderException.OTHER,"invalid comparison")
                    
        elif isinstance(side1,NodeBELONGS) and isinstance(side2,NodeNUMBER):
            return "score of %s > %d" % (side1.selection().master().element_name,side2.leaf)
        elif isinstance(side1,NodeNUMBER) and isinstance(side2,NodeBELONGS):
            return "score of %s > %d" % (side2.selection().master().element_name,side1.leaf)        
        else:
            raise RenderException(RenderException.OTHER,"invalid types")
            
    def luthor_greater_than_or_equal(self,side1,side2):        
        if isinstance(side1,NodeBELONGS) and isinstance(side2,NodeBELONGS):
            if side1.master().element_name!=side2.master().element_name:
                raise RenderException(RenderException.COULD_NOT_GROUP,"could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name))
                return "could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
            else:
                if side1.data_type=="NUMBER" and side1.data_type=="NUMBER":
                    return "%s loses or at most equalizes against %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
                elif side1.data_type=="STRING" and side1.data_type=="STRING":
                    raise RenderException(RenderException.OTHER,"invalid comparison")
                    
        elif isinstance(side1,NodeBELONGS) and isinstance(side2,NodeNUMBER):
            return "score of %s >= %d" % (side1.selection().master().element_name,side2.leaf)
        elif isinstance(side1,NodeNUMBER) and isinstance(side2,NodeBELONGS):
            return "score of %s >= %d" % (side2.selection().master().element_name,side1.leaf)        
        else:
            raise RenderException(RenderException.OTHER,"invalid types")     
            
    def luthor_render_group(self,node,group):
        ret="For %s :\n" % group["name"]
        for comparison in group["list"]:
            ret+= "- " + str(comparison.speech) + "\n"
        return ret
        

        
def link_groups(node,groups):        
    ret="\n".join(groups)    
    return ret
    
def link_nodes(master_node,slave_nodes):
    if isinstance(master_node,NodeAND):
        linker="\nand\n"
        ret = "[All of the follwing must be true]\n"
    elif isinstance(master_node,NodeOR):
        linker="\nor\n"
        ret = "[Just one of the following can be true]\n"
    else:
        raise RenderException(RenderException.OTHER,"invalid type")
    
    ret+=master_node.speech    
    if len(slave_nodes):
        for slave_node in slave_nodes:            
            ret+= linker+"(" + str(slave_node.speech) + ")"
        
        
    return ret
    
class TestSemantic(unittest.TestCase):
    
        
    def setUp(self):
        self.lexer = Lexer()
        self.lexer.build()
        self.parser = Parser(lexer=self.lexer)
        self.parser.build()
        
        self.universe = {
            "#link_groups#" : link_groups,
            "#link_nodes#" : link_nodes,
            "#constant#" : Constant(),
            "OMPSG" : {
                    "#name#" : "OM vs PSG",
                    "#type#" : "competition",
                    "#object#" : Competition(),
                    "OM" : {
                            "#name#" : "Marseille",
                            "#type#" : "side",
                            "score" : 1,
                            "best_player": "sytchev"
                          },
                    "PSG" : {
                            "#name#" : "Paris",
                            "#type#" : "side",
                            "score" : 1,
                            "best_player": "bob"
                          },
                    "caca" : {
                            "#name#" : "gros caca",
                            "#type#" : "side",
                            "score" : 3,
                            "best_player": "bob"
                    }
                },
            "LILLYO" : {
                    "#name#" : "Lille vs Lyon",
                    "#type#" : "competition",
                    "#object#" : Competition(),
                    "LILLE" : {
                            "#name#" : "Lille",
                            "#type#" : "side",
                            "score" : 2,
                            "best_player": "sytchev"
                          },
                    "LYON" : {
                            "#name#" : "Lyon",
                            "#type#" : "side",
                            "score" : 0,
                            "best_player": "bob"
                          }
                }
        }
    
    def write_to_file(self,filename,tree):
        f = open("test_out/"+filename+".gv", 'w')
        f.write('graph ast {')
        f.write(tree.dot())
        f.write('}')
    
    def test_simple_compare_1(self):
        tree = self.parser.parse('1=1')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_compare_1",tree);
        self.assertTrue(value)
        
    def test_simple_compare_2(self):
        tree = self.parser.parse('1<1')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_compare_2",tree);
        self.assertFalse(value)
        
    def test_simple_compare_3(self):
        tree = self.parser.parse('1>1')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_compare_3",tree);
        self.assertFalse(value)
        
    def test_simple_compare_4(self):
        tree = self.parser.parse('1<=1')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_compare_4",tree);
        self.assertTrue(value)
        
    def test_simple_compare_5(self):
        tree = self.parser.parse('1>=1')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_compare_5",tree);
        self.assertTrue(value)
        
    def test_simple_bool_expr_1(self):
        tree = self.parser.parse('1=2 OR 1=1')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_bool_expr_1",tree);
        self.assertTrue(value)
        
    def test_simple_bool_expr_2(self):
        tree = self.parser.parse('1=2 AND 1=1')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_bool_expr_2",tree);
        self.assertFalse(value)
    
    def test_simple_bool_expr_and_1(self):
        tree = self.parser.parse('1=1 AND 2=2 AND 3=3 AND 4=4 AND 5=5')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_bool_expr_and_1",tree);
        self.assertTrue(value)    
        
    def test_simple_bool_expr_and_2(self):
        tree = self.parser.parse('(1=1 AND 2=2 ) AND (3=3 AND (4=4 AND 5=5))')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_bool_expr_and_2",tree);
        self.assertTrue(value) 
    
    def test_complex_bool_expr_1(self):
        tree = self.parser.parse('(1=2 OR 1=1) AND 1=1 OR 1=2')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_complex_bool_expr_1",tree);
        self.assertTrue(value)
        
    def test_simple_bool_expr_and_selects_1(self):
        tree = self.parser.parse('OMPSG.PSG.score=1 AND OMPSG.OM.score=1 AND LILLYO.LILLE.score=2 AND LILLYO.LYON.score=0')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_bool_expr_and_selects_1",tree);
        self.assertTrue(value)
    
    def test_simple_bool_expr_and_or_selects_1(self):
        tree = self.parser.parse('OMPSG.PSG.score=1 AND OMPSG.OM.score=1 AND LILLYO.LILLE.score=3 OR LILLYO.LYON.score=0')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_bool_expr_and_or_selects_1",tree);
        self.assertTrue(value)
        
    def test_simple_bool_expr_and_or_selects_2(self):
        tree = self.parser.parse('OMPSG.PSG.score=OMPSG.OM.score AND LILLYO.LILLE.score=LILLYO.LYON.score')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_bool_expr_and_or_selects_2",tree);
        self.assertFalse(value)
        
    def test_simple_bool_expr_and_or_selects_3(self):
        tree = self.parser.parse('OMPSG.PSG.score=OMPSG.OM.score AND LILLYO.LILLE.score=LILLYO.LYON.score AND LILLYO.LILLE.score=1 AND LILLYO.LYON.score=0 AND OMPSG.OM.score=1')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_bool_expr_and_or_selects_3",tree);
        self.assertFalse(value)
        
    def test_simple_bool_expr_and_or_selects_4(self):
        tree = self.parser.parse('OMPSG.PSG.score=OMPSG.OM.score OR LILLYO.LILLE.score=LILLYO.LYON.score OR LILLYO.LILLE.score=1 OR LILLYO.LYON.score=0 AND OMPSG.OM.score=1')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_bool_expr_and_or_selects_4",tree);
        self.assertTrue(value)
    
    def test_simple_bool_expr_and_or_selects_5(self):
        tree = self.parser.parse('OMPSG.PSG.score=OMPSG.OM.score OR LILLYO.LILLE.score=LILLYO.LYON.score OR LILLYO.LILLE.score=1 AND LILLYO.LYON.score=0 OR OMPSG.OM.score=1')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_bool_expr_and_or_selects_5",tree);
        self.assertTrue(value)
        
    def test_simple_bool_expr_and_or_selects_6(self):
        tree = self.parser.parse('OMPSG.OM.score=1 AND OMPSG.PSG.score=0 OR (LILLYO.LILLE.score > LILLYO.LYON.score)')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_simple_bool_expr_and_or_selects_6",tree);
        self.assertTrue(value)
    
    def test_assoc_1(self):
        tree = self.parser.parse('(1=2 OR 1=1) AND 1=1 OR 1=2 AND 1>2')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_assoc_1",tree);
        self.assertTrue(value)
    
    def test_select_1(self):
        tree = self.parser.parse('OMPSG.OM.score=1')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_select_1",tree);
        self.assertTrue(value)
    
    def test_undefined_1(self):
        tree = self.parser.parse('Doom1=1')
        try:
            value = Interpreter(tree).execute(self.universe)
            self.write_to_file("test_undefined_1",tree);
        except SemanticException as e:
            self.assertEqual(e.code,SemanticException.NOT_DEFINED)
            
    def test_undefined_2(self):
        tree = self.parser.parse('Doom1.OM=1')
        try:
            value = Interpreter(tree).execute(self.universe)
            self.write_to_file("test_undefined_2",tree);
        except SemanticException as e:
            self.assertEqual(e.code,SemanticException.NOT_DEFINED)
            
    def test_select_fail_1(self):
        tree = self.parser.parse('OMPSG.PSG.score.bla=1')
        try:
            value = Interpreter(tree).execute(self.universe)
            self.write_to_file("test_select_fail_1",tree);
        except SemanticException as e:
            self.assertEqual(e.code,SemanticException.NO_SUBMEMBERS)
            
    def test_select_fail_2(self):
        tree = self.parser.parse('OMPSG.PSG=1')
        try:
            value = Interpreter(tree).execute(self.universe)
            self.write_to_file("test_select_fail_2",tree);
        except SemanticException as e:
            self.assertEqual(e.code,SemanticException.SIMPLE_TYPE_EXPECTED)
            
    def test_select_fail_3(self):
        tree = self.parser.parse('OMPSG.PSG=1')
        try:
            value = Interpreter(tree).execute(self.universe)
            self.write_to_file("test_select_fail_3",tree);
        except SemanticException as e:
            self.assertEqual(e.code,SemanticException.SIMPLE_TYPE_EXPECTED)
     
    def test_complex_1(self):
        tree = self.parser.parse('1<1 AND \
                            (\
                                OMPSG.PSG.best_player = OMPSG.caca.best_player AND OMPSG.OM.score >= 1 AND \
                                    ("bob" = OMPSG.PSG.best_player OR OMPSG.OM.score = OMPSG.PSG.score) AND\
                                    (OMPSG.OM.best_player = OMPSG.PSG.best_player OR OMPSG.OM.score = OMPSG.PSG.score OR 1 = OMPSG.OM.score)\
                                AND OMPSG.OM.best_player = "sytchev"\
                                )')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_complex_1",tree);
        self.assertFalse(value)
        
    def test_complex_2(self):
        tree = self.parser.parse('1<=2 AND \
                            (\
                                OMPSG.PSG.best_player = OMPSG.caca.best_player AND OMPSG.OM.score >= 1 AND \
                                    ("bob" = OMPSG.PSG.best_player OR OMPSG.OM.score = OMPSG.PSG.score) AND\
                                    (OMPSG.OM.best_player = OMPSG.PSG.best_player OR OMPSG.OM.score = OMPSG.PSG.score OR 1 = OMPSG.OM.score)\
                                AND OMPSG.OM.best_player = "sytchev"\
                                )')
        value = Interpreter(tree).execute(self.universe)
        self.write_to_file("test_complex_2",tree);
        self.assertTrue(value)
    
unittest.main()
