from tree import Node,NodeBELONGS,NodeIDENTIFIER,NodeConstant,NodeOR,NodeBoolOp,NodeComparisonOp
from errors import SemanticException,RenderException

class Interpreter:    
    def __init__(self,tree):
        self.tree = tree
        self.grouping_successful = True 
        
    def eval_symbol(self, node, univ):  
        if node.type == "BELONGS":
            if node.master().leaf in univ:
                if isinstance(univ[node.master().leaf], dict):
                    #check if child exists
                    if node.selection().type == "BELONGS":                        
                        if not (node.selection().master().leaf in univ[node.master().leaf]):
                            raise SemanticException(SemanticException.BAD_MEMBER, "%s is not a member of %s" % (node.selection().master().leaf,node.master().leaf))
                    elif node.selection().type == "IDENTIFIER":
                        if not (node.selection().leaf in univ[node.master().leaf]):
                            raise SemanticException(SemanticException.BAD_MEMBER,"%s is not a member of %s" % (node.selection().leaf,node.master().leaf))
                    else:
                        raise SemanticException(SemanticException.INTERNAL_WRONG_NODE, "Bad node type. The node is %s" % node.selection().type)
                    
                    self.eval_symbol(node.selection(),univ[node.master().leaf])
                    if "#type#" in univ[node.master().leaf] and "#name#" in univ[node.master().leaf]:
                        node.master().element_type = univ[node.master().leaf]["#type#"]
                        node.master().element_name = univ[node.master().leaf]["#name#"]
                    else:
                        raise RenderException(RenderException.BAD_STRUCTURE,"no #type# or #name# identifiers in sub-universe: %s" % univ[node.master().leaf])
                    node.master().object = univ[node.master().leaf].get("#object#",None)
                    node.object = node.master().object
                    node.data_type = node.selection().data_type
                    node.value = node.selection().value
                else:
                    raise SemanticException(SemanticException.NO_SUBMEMBERS,"%s has no submembers" % node.master().leaf)
            else:
                #root symbol invalid
                raise SemanticException(SemanticException.NOT_DEFINED, "symbol %s is not defined" % node.master().leaf)
        elif node.type == "IDENTIFIER":
            if node.leaf in univ:
                if not ("#value#" in univ[node.leaf]):
                    raise SemanticException(SemanticException.SIMPLE_TYPE_EXPECTED,"symbol %s is not a simple type" % node.leaf)
                else:
                    if "#type#" in univ[node.leaf]:
                        if univ[node.leaf]["#type#"] == "NUMBER" or univ[node.leaf]["#type#"] == "STRING":                        
                            node.data_type = univ[node.leaf]["#type#"]
                        else:
                            raise SemanticException(SemanticException.UNKNOWN_DATA_TYPE,"symbol %s is of unknown type %s. Check universe definition for this symbol." % node.leaf,univ[node.leaf]["#type#"])
                    else:
                        raise SemanticException(SemanticException.NO_DATA_TYPE,"symbol %s is has no data time defined. Check universe definition for this symbol." % node.leaf)
                    node.value = univ[node.leaf]["#value#"]
            else:
                raise SemanticException(SemanticException.NOT_DEFINED, "symbol %s is not defined" % node.leaf)
        elif node.type == "STRING":
            self.eval_constant(node)
        elif node.type == "NUMBER":
            self.eval_constant(node)
    def eval_constant(self,node):        
        node.value = node.leaf
        node.object = self.constant_object
    
    def catch_grouping_exceptions(self,function,*args):
        try:
            return function(*args)
        except RenderException as e:
            if e.code == RenderException.COULD_NOT_GROUP:
                self.grouping_successful = False
                return e.value
            else:
                raise
                
    def eval_comparison(self, node, univ): 
        self.eval_symbol(node.left(),univ) 
        self.eval_symbol(node.right(),univ)
        #propagate object from the left. If nonavailable, propagate it from the right
        node.object = node.left().object
        if node.object is None:
            node.object = node.right().object
        
        lhs_data = node.left().value
        rhs_data = node.right().value
        
        #if node.parent is None:
        node.map = dict()
        map = node.map
        #else:
        #    map = node.parent.map

        if node.left().type == "BELONGS" or node.right().type == "BELONGS":
            if node.left().type == "BELONGS":
                mapped_node = node.left().master().leaf
                mapped_node_name = node.left().master().element_name
                #There are two cases. 
                #If the mapped_node is already present: we are adding the same competition (the node is BELONGS), overwriting is OK
                #If the mapped_node doesnt exist we add it, obviously
                #to give some examples 1) OMPSG.OM.score>OMPSG.PSG.score 2) OMPSG.OM.score > LILLELYON.LILLE.score 
                map[mapped_node] = {"list":[node],"name":mapped_node_name,"object":node.object}
                    
            if node.right().type == "BELONGS":
                mapped_node = node.right().master().leaf
                mapped_node_name = node.right().master().element_name                
                #There are two cases. 
                #If the mapped_node is already present: we are adding the same competition (the node is BELONGS), overwriting is OK
                #If the mapped_node doesnt exist we add it, obviously
                #to give some examples 1) OMPSG.OM.score>OMPSG.PSG.score 2) OMPSG.OM.score > LILLELYON.LILLE.score           
                map[mapped_node] = {"list":[node],"name":mapped_node_name,"object":node.object}
        else:
            mapped_node = "#misc#"
            mapped_node_name = "#misc#"
            
            if mapped_node in map:
                map[mapped_node]["list"].append(node)
            else:            
                map[mapped_node] = {"list":[node],"name":mapped_node_name,"object":node.object}
        
        if node.left().data_type == node.right().data_type:
            if node.type=="EQUAL":
                node.value = (lhs_data == rhs_data)
                if not(node.object is None):                    
                    node.speech = self.catch_grouping_exceptions(node.object.luthor_equals,node.left(),node.right())
                    
            else:
                if node.left().data_type == "NUMBER": #can only compare numbers
                    if node.type=="GREATER_THAN_OR_EQUAL":
                        node.value = (lhs_data >= rhs_data)
                        if not(node.object is None):                    
                            node.speech = self.catch_grouping_exceptions(node.object.luthor_greater_than_or_equal,node.left(),node.right())
                    elif node.type=="GREATER_THAN":
                        node.value = (lhs_data > rhs_data)
                        if not(node.object is None):                    
                            node.speech = self.catch_grouping_exceptions(node.object.luthor_greater_than,node.left(),node.right())
                    elif node.type=="LESS_THAN_OR_EQUAL":
                        node.value = (lhs_data <= rhs_data)
                        if not(node.object is None):                    
                            node.speech = self.catch_grouping_exceptions(node.object.luthor_less_than_or_equal,node.left(),node.right())
                    elif node.type=="LESS_THAN":
                        node.value = (lhs_data < rhs_data)
                        if not(node.object is None):                    
                            node.speech = self.catch_grouping_exceptions(node.object.luthor_less_than,node.left(),node.right())
                    else:
                        raise SemanticException(SemanticException.INTERNAL_WRONG_NODE, "Bad node type. The node is %s" % node.type)
                else:
                    raise SemanticException(SemanticException.NUMBER_EXPECTED_IN_COMPARE, "It is only possible to compare numbers. You are comparing objects of type %s" % (node.left().data_type))
        else:
            raise SemanticException(SemanticException.TYPE_MISMATCH, "type mismatch in comparison. Cannot compare %s and %s" % (node.left().data_type,node.right().data_type))
    
    def eval_bool_expression_side(self,node, univ):
        if isinstance(node,NodeBoolOp):
            self.eval_bool_expression(node,univ)
        elif isinstance(node, NodeComparisonOp):
            self.eval_comparison(node,univ)
        else:
            raise SemanticException(SemanticException.INTERNAL_WRONG_NODE, "Bad node type. The node is %s" % node.type)
    
    def update_map(self,node,child):     
        for child_key in child.map.keys():
            if child_key in node.map.keys():
                node.map[child_key]["list"].append(child)
            else:
                node.map[child_key] = {"list":[child],"name":child.map[child_key]["name"],"object":child.map[child_key]["object"]}
    
    def map_with_filtered_list(self,map,node_type):
        return {"name":map["name"],"object":map["object"],"list":filter(lambda node: isinstance(node,node_type),map["list"])}
    
    def get_group_list(self,node):
        group_list = []
        for key in node.map.keys():
            if ("object" in node.map[key]) and not (node.map[key]["object"] is None):
                filtered_map = self.map_with_filtered_list(node.map[key],NodeComparisonOp)
                if len(filtered_map["list"]) > 0:
                    group_list.append(node.map[key]["object"].luthor_render_group(node,filtered_map))
        return group_list
       
    def eval_bool_expression(self,node, univ):
        if node.type == "AND":
            node.value = True            
            for child in node.children:
                self.eval_bool_expression_side(child,univ)
                node.value = node.value and child.value
                self.update_map(node, child)
                        
            group_list = self.get_group_list(node)            
            if len(group_list)==0:
                node.speech = node.children[0].speech
                node.speech = self.link_nodes(node,filter(lambda child: isinstance(child,NodeBoolOp),node.children[1:]))
            else:
                node.speech = self.link_groups(node,group_list)
                node.speech = self.link_nodes(node,filter(lambda child: isinstance(child,NodeBoolOp),node.children))
                
        elif node.type == "OR":
            node.value = False
            for child in node.children:
                self.eval_bool_expression_side(child,univ)
                node.value = node.value or child.value                
                self.update_map(node, child)
            
            group_list = self.get_group_list(node)            
            if len(group_list)==0:
                node.speech = node.children[0].speech
                node.speech = self.link_nodes(node,filter(lambda child: isinstance(child,NodeBoolOp),node.children[1:]))
            else:
                node.speech = self.link_groups(node,group_list)
                node.speech = self.link_nodes(node,filter(lambda child: isinstance(child,NodeBoolOp),node.children)) 
        else:
            raise SemanticException(SemanticException.INTERNAL_WRONG_NODE, "Bad node type. The node is %s" % node.type)
        
    def eval(self, node, univ):
        if node.type == "BELONGS" or node.type=="IDENTIFIER":
            self.eval_symbol(node, univ)
        elif isinstance(node, NodeComparisonOp):
            self.eval_comparison(node, univ)
        elif isinstance(node,NodeBoolOp):
            self.eval_bool_expression(node, univ)
        else:
            raise SemanticException(SemanticException.INTERNAL_WRONG_NODE, "Bad node type. The node is %s" % node.type)
        
    def execute(self,universe):
        self.link_groups = universe["#link_groups#"];        
        self.link_nodes= universe["#link_nodes#"];
        self.constant_object = universe["#constant#"];
        if self.tree is None:
            return None
        self.eval(self.tree,universe)
        return self.tree.value
