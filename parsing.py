import ply.yacc as yacc

from lexing import Lexer
from tree import Node,NodeBELONGS,NodeIDENTIFIER,NodeEQUAL,NodeGREATER_THAN_OR_EQUAL,NodeGREATER_THAN,NodeLESS_THAN_OR_EQUAL,NodeLESS_THAN,NodeNUMBER,NodeSTRING,NodeAND,NodeOR
from errors import ParseException

class Parser:
    def __init__(self,lexer):
        self.lexer = lexer
        
    precedence = (        
        ('right', 'BELONGS'),
        ('left', 'OR'),
        ('left', 'AND'),
    )
    
    def p_expression_expression_and_expression(self,p):    
        'expression : expression AND expression'
        if(isinstance(p[1],NodeAND)):
            p[0] = p[1]
            p[0].children.append(p[3])
            p[3].parent = p[0]            
        else:
            p[0] = NodeAND(children=[p[1],p[3]], leaf=p[2])            
            p[1].parent = p[0]
            p[3].parent = p[0]
        
    def p_expression_expression_or_expression(self,p):    
        'expression : expression OR expression'                
        if(isinstance(p[1],NodeOR)):
            p[0] = p[1]
            p[0].children.append(p[3])
            p[3].parent = p[0]            
        else:
            p[0] = NodeOR(children=[p[1],p[3]], leaf=p[2])
            p[1].parent = p[0]
            p[3].parent = p[0]
        
    def p_expression_paren(self,p):
        'expression : LPAREN expression RPAREN'
        p[0] = p[2] 
        
    def p_expression_symbol(self,p):
        'expression : comparison'
        p[0] = p[1]    
        
    def p_comparison_symbol_eq_symbol(self,p):    
        'comparison : symbol EQUAL symbol'
        p[0] = NodeEQUAL(children=[p[1],p[3]], leaf=p[2])
        p[1].parent = p[0]
        p[3].parent = p[0]
        
    def p_comparison_symbol_gteq_symbol(self,p):    
        'comparison : symbol GREATER_THAN_OR_EQUAL symbol'
        p[0] = NodeGREATER_THAN_OR_EQUAL(children=[p[1],p[3]], leaf=p[2])
        p[1].parent = p[0]
        p[3].parent = p[0]
        
    def p_comparison_symbol_gt_symbol(self,p):    
        'comparison : symbol GREATER_THAN symbol'
        p[0] = NodeGREATER_THAN(children=[p[1],p[3]], leaf=p[2])
        p[1].parent = p[0]
        p[3].parent = p[0]
        
    def p_comparison_symbol_lteq_symbol(self,p):    
        'comparison : symbol LESS_THAN_OR_EQUAL symbol'
        p[0] = NodeLESS_THAN_OR_EQUAL(children=[p[1],p[3]], leaf=p[2])
        p[1].parent = p[0]
        p[3].parent = p[0]
        
    def p_comparison_symbol_lt_symbol(self,p):    
        'comparison : symbol LESS_THAN symbol'
        p[0] = NodeLESS_THAN(children=[p[1],p[3]], leaf=p[2])
        p[1].parent = p[0]
        p[3].parent = p[0]
        
    def p_symbol_object(self,p):
        'symbol : object'
        p[0] = p[1]        
       
    def p_symbol_string(self,p):
        'symbol : STRING'
        p[0] = NodeSTRING(leaf=p[1])
        
    def p_symbol_number(self,p):
        'symbol : NUMBER'
        p[0] = NodeNUMBER(leaf=p[1])
        
    def p_object_belongs_object(self,p):
        'object : object BELONGS object'        
        p[0] = NodeBELONGS(children=[p[1],p[3]], leaf=p[2])
        p[1].parent = p[0]
        p[3].parent = p[0]

    def p_object_identifier(self,p):
        'object : IDENTIFIER'
        p[0] = NodeIDENTIFIER(leaf=p[1])

    

    # Error rule for syntax errors
    def p_error(self,p):
        raise ParseException(p)        

    def build(self,**kwargs):
        self.tokens = Lexer.tokens        
        self.parser = yacc.yacc(module=self,optimize=1, **kwargs)
        
    # Test it output
    def parse(self,data):        
        return self.parser.parse(data,lexer=self.lexer.lexer)
