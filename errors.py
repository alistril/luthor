class LuthorException(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)
        
class LexException(LuthorException):
    pass
    
class ParseException(LuthorException):
    pass
  
class SemanticException(LuthorException):
    TYPE_MISMATCH = 1
    BAD_MEMBER = 2
    NO_SUBMEMBERS = 3
    NOT_DEFINED = 4
    SIMPLE_TYPE_EXPECTED = 5
    NUMBER_EXPECTED_IN_COMPARE = 6
    UNKNOWN_DATA_TYPE = 7
    NO_DATA_TYPE = 8
    INTERNAL_WRONG_NODE = 100    
    
    def __init__(self, code, value):
        self.value = value
        self.code = code
        
    def __str__(self):
        return repr(self.value) + "[" + str(self.code) + "]"
        
class RenderException(LuthorException):
    COULD_NOT_GROUP = 1
    BAD_STRUCTURE = 2
    OTHER = 3
    def __init__(self, code, value):
        self.value = value
        self.code = code
        
    def __str__(self):
        return repr(self.value) + "[" + str(self.code) + "]"