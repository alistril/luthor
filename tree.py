import uuid

class Node(object):
    def __init__(self,type,children=None,leaf=None):
         self.type = type
         if children:
              self.children = children
         else:
              self.children = [ ]
         self.leaf = leaf         
         self.id = int(uuid.uuid1())
         self.parent = None
         self.value= None
         self.object = None
    
    def __str__(self):
        return str(self.leaf)
    
    def dot_me(self):
        val = ""        
        if not (self.value is None):
            val = "[" + str(self.value) + "]"        
        
        return str(self.id) + '[label="' + str(self) + val + '"];'
    
        
    def dot(self):
        ret=""
        ret+= self.dot_me()        
        ret+="\n"
        for child in self.children:            
            ret+= str(child.id) + '[label="' + str(child) + '"];'
            ret+="\n"
            ret+= str(self.id) + " -- " + str(child.id) + ";"
            ret+="\n"
            ret+=child.dot()
            
        return ret

class NodeSYMBOL(Node):
    def __init__(self,type,children=None,leaf=None,data_type=None):
        super(NodeSYMBOL,self).__init__(type,children=children,leaf=leaf)
        self.data_type = data_type
        
class NodeConstant(NodeSYMBOL):
    def __init__(self,type,leaf=None):
        super(NodeConstant,self).__init__(type,children=None,leaf=leaf,data_type=type)
        
class NodeBinaryOp(Node):
    def left(self):
        return self.children[0]
        
    def right(self):
        return self.children[1]

class NodeBoolOp(Node):
    def __init__(self,type,children=None,leaf=None):
        super(NodeBoolOp,self).__init__(type,children=children,leaf=leaf)
        self.map = dict()
        
    def dot_me(self):
        val = ""
        if not (self.value is None):
            val = "[" + str(self.value) + "]"
            
        if not (self.speech is None):
            speech = "\n[" + str(self.speech) + "]"
        return str(self.id) + '[label="' + str(self) + val + "\\n"+ str([self.map[key]["name"] for key in self.map.keys()]) + speech.replace("\n","\\n") + '"];'
    
class NodeComparisonOp(NodeBinaryOp):
    def __init__(self,type,children=None,leaf=None):
        super(NodeComparisonOp,self).__init__(type,children=children,leaf=leaf)
        self.speech = None
        
    def dot_me(self):
        val = ""
        speech=""
        if not (self.value is None):
            val = "[" + str(self.value) + "]"
            
        if not (self.speech is None):
            speech = "\n[" + str(self.speech) + "]"
        return str(self.id) + '[label="' + str(self) + val + speech.replace("\n","\\n") + '"];'
        
class NodeBELONGS(NodeSYMBOL):
    def __init__(self,children=None,leaf=None,data_type=None):
        super(NodeBELONGS,self).__init__("BELONGS",children=children,leaf=leaf)
        self.data_type = data_type
    
    def dot_me(self):
        val = ""        
        if not (self.value is None):
            val = "[" + str(self.value) + "]"
        if self.data_type is None:
            data_type = "?"
        else:
            data_type = self.data_type
            
        return str(self.id) + '[label="' + str(self) + "(" + data_type + ")" + val + '"];' 
        
    def master(self):
        return self.children[0]
        
    def selection(self):
        return self.children[1]
        
    def parent_master(self):
        if self.parent is None:
            return None
        return self.parent.master()
        
class NodeIDENTIFIER(NodeSYMBOL):
    def dot_me(self):
        val = ""
        if not (self.value is None):
            val = "[" + str(self.value) + "]"
            
        if self.data_type is None:
            data_type = "?"
        else:
            data_type = self.data_type
            
        if self.element_type is None:
            element_type = ""
        else:
            element_type = "\\ntype: %s" % self.element_type
            
        if self.element_name is None:
            element_name = ""
        else:
            element_name = "\\nname: %s" % self.element_name
            
        return str(self.id) + '[label="' + str(self) + "(" + data_type + ")" + val + element_type + element_name + '"];' 
        
    def __init__(self,leaf=None,data_type=None):
        super(NodeIDENTIFIER,self).__init__("IDENTIFIER",children=None,leaf=leaf,data_type=None)
        self.element_name = None
        self.element_type = None
        
class NodeEQUAL(NodeComparisonOp):
    def __init__(self,children=None,leaf=None,parent=None):
        super(NodeEQUAL,self).__init__("EQUAL",children=children,leaf=leaf)
        
class NodeGREATER_THAN(NodeComparisonOp):
    def __init__(self,children=None,leaf=None,parent=None):
        super(NodeGREATER_THAN,self).__init__("GREATER_THAN",children=children,leaf=leaf)
        
class NodeGREATER_THAN_OR_EQUAL(NodeComparisonOp):
    def __init__(self,children=None,leaf=None,parent=None):
        super(NodeGREATER_THAN_OR_EQUAL,self).__init__("GREATER_THAN_OR_EQUAL",children=children,leaf=leaf)

class NodeLESS_THAN(NodeComparisonOp):
    def __init__(self,children=None,leaf=None,parent=None):
        super(NodeLESS_THAN,self).__init__("LESS_THAN",children=children,leaf=leaf)
        
class NodeLESS_THAN_OR_EQUAL(NodeComparisonOp):
    def __init__(self,children=None,leaf=None,parent=None):
        super(NodeLESS_THAN_OR_EQUAL,self).__init__("LESS_THAN_OR_EQUAL",children=children,leaf=leaf)

        
class NodeSTRING(NodeConstant):
    def __init__(self,leaf=None,parent=None):
        super(NodeSTRING,self).__init__(type="STRING",leaf=leaf)
    
class NodeNUMBER(NodeConstant):
    def __init__(self,leaf=None,parent=None):
        super(NodeNUMBER,self).__init__(type="NUMBER",leaf=leaf)

class NodeAND(NodeBoolOp):
    def __init__(self,children=None,leaf=None):
        super(NodeAND,self).__init__("AND",children=children,leaf=leaf)
        
class NodeOR(NodeBoolOp):
    def __init__(self,children=None,leaf=None):
        super(NodeOR,self).__init__("OR",children=children,leaf=leaf)
        
